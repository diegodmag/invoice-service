package com.invoiceservice.exceptionHandling;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.invoiceservice.exceptionHandling.ApiException;
import com.invoiceservice.exceptionHandling.ExceptionResponse;

@ControllerAdvice 
//Permite declarar metodos relacionados con el manejo de excepciones entre distintos controladores 
public class RestExceptionHandler extends ResponseEntityExceptionHandler{
	//Entre otras cosas la clase ResponseEntityExceptionHandler permite definir excepciones personalizadas entre otras cosas 
	
	@ExceptionHandler(ApiException.class)
	protected ResponseEntity<Object> handleApiException(ApiException exception, WebRequest request){
		
		ExceptionResponse response = new ExceptionResponse();
		response.setMessage(exception.getMessage());
		response.setStatus(exception.getStatus().value());
		response.setError(exception.getStatus());
		response.setPath(((ServletWebRequest)request).getRequest().getRequestURI().toString());
		
		return new ResponseEntity<>(response, response.getError());
		
	}
	
	
	
	
	
}
